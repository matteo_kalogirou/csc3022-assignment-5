#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include "catch.hpp"
#include "samp.h"

using namespace std;

TEST_CASE("Construction from vector", ""){
    vector<int8_t> input{-20, -10, -5, 0, 5, 50, 100};
    vector<int16_t> input16{-20, -10, -5, 0, 5, 50, 100};
    vector<int8_t> pin{-1,2,5};

    Audio<int8_t> A(input);
    Audio<int16_t> B(input16);
    Audio< pair<int8_t, int8_t> > P(pin);

    REQUIRE(input[0] == A.get(0) );
    REQUIRE(input[1] == A.get(1) );
    REQUIRE(input[2] == A.get(2) );
    REQUIRE(input[3] == A.get(3) );
    REQUIRE(input[4] == A.get(4) );
    REQUIRE(input[5] == A.get(5) );
    REQUIRE(input[6] == A.get(6) );

    REQUIRE( P.getL(0) == pin[0] );
    REQUIRE( P.getR(0) == pin[0]*2 );
    REQUIRE( P.getL(1) == pin[1] );
    REQUIRE( P.getR(1) == pin[1]*2 );
    REQUIRE( P.getL(2) == pin[2] );
    REQUIRE( P.getR(2) == pin[2]*2 );

    REQUIRE(input16[0] == B.get(0) );
    REQUIRE(input16[1] == B.get(1) );
    REQUIRE(input16[2] == B.get(2) );
    REQUIRE(input16[3] == B.get(3) );
    REQUIRE(input16[4] == B.get(4) );
    REQUIRE(input16[5] == B.get(5) );
    REQUIRE(input16[6] == B.get(6) );

}

TEST_CASE("Copy Constructor"){
    vector<int8_t> input{-20, -10, -5, 0, 5, 50, 100};
    vector<int16_t> input16{-20, -10, -5, 0, 5, 50, 100};
    vector<int8_t> pin{-1,2,5};

    Audio<int8_t> A(input);
    Audio<int16_t> B(input16);
    Audio< pair<int8_t, int8_t> > P(pin);

    Audio<int8_t> C(A);         //Copy Construct
    Audio<int16_t> D(B);
    Audio< pair<int8_t, int8_t> > Q(P);

    REQUIRE( C.get(0) == A.get(0) );
    REQUIRE( C.get(1) == A.get(1) );
    REQUIRE( C.get(2) == A.get(2) );
    REQUIRE( C.get(3) == A.get(3) );
    REQUIRE( C.get(4) == A.get(4) );
    REQUIRE( C.get(5) == A.get(5) );
    REQUIRE( C.get(6) == A.get(6) );

    REQUIRE( D.get(0) == B.get(0) );
    REQUIRE( D.get(1) == B.get(1) );
    REQUIRE( D.get(2) == B.get(2) );
    REQUIRE( D.get(3) == B.get(3) );
    REQUIRE( D.get(4) == B.get(4) );
    REQUIRE( D.get(5) == B.get(5) );
    REQUIRE( D.get(6) == B.get(6) );

    REQUIRE( P.getL(0) == Q.getL(0) );
    REQUIRE( P.getR(0) == Q.getR(0) );
    REQUIRE( P.getL(1) == Q.getL(1) );
    REQUIRE( P.getR(1) == Q.getR(1) );
    REQUIRE( P.getL(2) == Q.getL(2) );
    REQUIRE( P.getR(2) == Q.getR(2) );
}

TEST_CASE("Move Constructor"){
    vector<int8_t> input{-20, -10, -5, 0, 5, 50, 100};
    vector<int16_t> input16{-20, -10, -5, 0, 5, 50, 100};
    vector<int8_t> pin{-1,2,5};

    Audio<int8_t> A(input);
    Audio<int16_t> B(input16);
    Audio< pair<int8_t, int8_t> > P(pin);


    Audio<int8_t> C(move(A));         //Move Construct
    Audio<int16_t> D(move(B));
    Audio< pair<int8_t, int8_t> > Q(move(P));


    REQUIRE(input[0] == C.get(0) );
    REQUIRE(input[1] == C.get(1) );
    REQUIRE(input[2] == C.get(2) );
    REQUIRE(input[3] == C.get(3) );
    REQUIRE(input[4] == C.get(4) );
    REQUIRE(input[5] == C.get(5) );
    REQUIRE(input[6] == C.get(6) );

    REQUIRE(input16[0] == D.get(0) );
    REQUIRE(input16[1] == D.get(1) );
    REQUIRE(input16[2] == D.get(2) );
    REQUIRE(input16[3] == D.get(3) );
    REQUIRE(input16[4] == D.get(4) );
    REQUIRE(input16[5] == D.get(5) );
    REQUIRE(input16[6] == D.get(6) );

    REQUIRE( Q.getL(0) == pin[0] );
    REQUIRE( Q.getR(0) == pin[0]*2 );
    REQUIRE( Q.getL(1) == pin[1] );
    REQUIRE( Q.getR(1) == pin[1]*2 );
    REQUIRE( Q.getL(2) == pin[2] );
    REQUIRE( Q.getR(2) == pin[2]*2 );
}

TEST_CASE("Copy Assignment"){
    vector<int8_t> input{-20, -10, -5, 0, 5, 50, 100};
    vector<int8_t> input2{-20, -10, 5, 50, 100};
    vector<int16_t> input16{-20, -10, -5, 0, 5, 50, 100};
    vector<int16_t> input162{-20, -10, -5, 0};

    vector<int8_t> pin{-1,2,5};
    vector<int8_t> ppin{-1,2,5,8,0};

    Audio<int8_t> A(input);
    Audio<int16_t> B(input16);
    Audio<int8_t> C(input2);
    Audio<int16_t> D(input162);
    Audio< pair<int8_t, int8_t> > P(pin);
    Audio< pair<int8_t, int8_t> > Q(ppin);

    C = A;         //Copy assignment
    D = B;
    Q = P;

    REQUIRE( C.get(0) == A.get(0) );
    REQUIRE( C.get(1) == A.get(1) );
    REQUIRE( C.get(2) == A.get(2) );
    REQUIRE( C.get(3) == A.get(3) );
    REQUIRE( C.get(4) == A.get(4) );
    REQUIRE( C.get(5) == A.get(5) );
    REQUIRE( C.get(6) == A.get(6) );

    REQUIRE( D.get(0) == B.get(0) );
    REQUIRE( D.get(1) == B.get(1) );
    REQUIRE( D.get(2) == B.get(2) );
    REQUIRE( D.get(3) == B.get(3) );
    REQUIRE( D.get(4) == B.get(4) );
    REQUIRE( D.get(5) == B.get(5) );
    REQUIRE( D.get(6) == B.get(6) );

    REQUIRE( P.getL(0) == Q.getL(0) );
    REQUIRE( P.getR(0) == Q.getR(0) );
    REQUIRE( P.getL(1) == Q.getL(1) );
    REQUIRE( P.getR(1) == Q.getR(1) );
    REQUIRE( P.getL(2) == Q.getL(2) );
    REQUIRE( P.getR(2) == Q.getR(2) );
}

TEST_CASE("Move Assignment"){
    vector<int8_t> input{-20, -10, -5, 0, 5, 50, 100};
    vector<int8_t> input2{-20, -10, 5, 50, 100};
    vector<int16_t> input16{-20, -10, -5, 0, 5, 50, 100};
    vector<int16_t> input162{-20, -10, -5, 0};
    vector<int8_t> pin{-1,2,5};
    vector<int8_t> ppin{-1,2,5,8,0};

    Audio<int8_t> A(input);
    Audio<int16_t> B(input16);
    Audio<int8_t> C(input2);
    Audio<int16_t> D(input162);
    Audio< pair<int8_t, int8_t> > P(pin);
    Audio< pair<int8_t, int8_t> > Q(ppin);    

    C = move(A);         //Move assignment
    D = move(B);
    Q = move(P);

    REQUIRE(input[0] == C.get(0) );
    REQUIRE(input[1] == C.get(1) );
    REQUIRE(input[2] == C.get(2) );
    REQUIRE(input[3] == C.get(3) );
    REQUIRE(input[4] == C.get(4) );
    REQUIRE(input[5] == C.get(5) );
    REQUIRE(input[6] == C.get(6) );

    REQUIRE(input16[0] == D.get(0) );
    REQUIRE(input16[1] == D.get(1) );
    REQUIRE(input16[2] == D.get(2) );
    REQUIRE(input16[3] == D.get(3) );
    REQUIRE(input16[4] == D.get(4) );
    REQUIRE(input16[5] == D.get(5) );
    REQUIRE(input16[6] == D.get(6) );

    REQUIRE( Q.getL(0) == pin[0] );
    REQUIRE( Q.getR(0) == pin[0]*2 );
    REQUIRE( Q.getL(1) == pin[1] );
    REQUIRE( Q.getR(1) == pin[1]*2 );
    REQUIRE( Q.getL(2) == pin[2] );
    REQUIRE( Q.getR(2) == pin[2]*2 );
}

TEST_CASE("Iterator ++"){
    vector<int8_t> input{-20, -10, -5, 0, 5, 50, 100};
    vector<int16_t> input16{-20, -10, -5, 0, 5, 50, 100};
    vector<int8_t> pin{-1,2,5};

    Audio<int8_t> A(input);
    vector<int8_t>::iterator Abeg = A.begin();
    vector<int8_t>::iterator Aend = A.end();

    Audio<int16_t> B(input16);
    vector<int16_t>::iterator Bbeg = B.begin();
    vector<int16_t>::iterator Bend = B.end();

    Audio< pair<int8_t, int8_t> > P(pin);
    Audio< pair<int8_t, int8_t> >::iterator Pbeg = P.begin();
    Audio< pair<int8_t, int8_t> >::iterator Pend = P.end();


    REQUIRE(input[0] == *Abeg ); ++Abeg;
    REQUIRE(input[1] == *Abeg ); ++Abeg;
    REQUIRE(input[2] == *Abeg ); ++Abeg;
    REQUIRE(input[3] == *Abeg ); ++Abeg;
    REQUIRE(input[4] == *Abeg ); ++Abeg;
    REQUIRE(input[5] == *Abeg ); ++Abeg;
    REQUIRE(input[6] == *Abeg ); ++Abeg;
    REQUIRE( *Aend == *Abeg );

    REQUIRE(input16[0] == *Bbeg ); ++Bbeg;
    REQUIRE(input16[1] == *Bbeg ); ++Bbeg;
    REQUIRE(input16[2] == *Bbeg ); ++Bbeg;
    REQUIRE(input16[3] == *Bbeg ); ++Bbeg;
    REQUIRE(input16[4] == *Bbeg ); ++Bbeg;
    REQUIRE(input16[5] == *Bbeg ); ++Bbeg;
    REQUIRE(input16[6] == *Bbeg ); ++Bbeg;
    REQUIRE( *Bend == *Bbeg );


    REQUIRE( (*Pbeg).first == pin[0] );
    REQUIRE( (*Pbeg).second == pin[0]*2 ); ++Pbeg;
    REQUIRE( (*Pbeg).first == pin[1] );
    REQUIRE( (*Pbeg).second == pin[1]*2 ); ++Pbeg;
    REQUIRE( (*Pbeg).first == pin[2] );
    REQUIRE( (*Pbeg).second == pin[2]*2 ); ++Pbeg;
    REQUIRE( Pbeg == Pend );
    

    
}

TEST_CASE("Concatenation |"){
    vector<int8_t> input{-20, -10, -5};
    vector<int8_t> input2{ 5, 50, 100};
    Audio<int8_t> A(input);
    Audio<int8_t> AA(input2);

    vector<int8_t> pin{-1,2,5};
    vector<int8_t> ppin{0,3,6};
    Audio< pair<int8_t, int8_t> > P(pin);
    Audio< pair<int8_t, int8_t> > PP(ppin);
    
    Audio<int8_t> C = A | AA;
    Audio<pair<int8_t, int8_t> > D = P | PP;

    auto beg = C.begin(), end = C.end();
    auto begD = D.begin();

    REQUIRE( *beg == input[0] ); ++beg;
    REQUIRE( *beg == input[1] ); ++beg;
    REQUIRE( *beg == input[2] ); ++beg;
    REQUIRE( *beg == input2[0] ); ++beg;
    REQUIRE( *beg == input2[1] ); ++beg;
    REQUIRE( *beg == input2[2] );

    REQUIRE( (*begD).first == pin[0] );
    REQUIRE( (*begD).second == pin[0]*2 ); ++begD;
    REQUIRE( (*begD).first == pin[1] );
    REQUIRE( (*begD).second == pin[1]*2 ); ++begD;
    REQUIRE( (*begD).first == pin[2] );
    REQUIRE( (*begD).second == pin[2]*2 ); ++begD;
    REQUIRE( (*begD).first == ppin[0] );
    REQUIRE( (*begD).second == ppin[0]*2 ); ++begD;
    REQUIRE( (*begD).first == ppin[1] );
    REQUIRE( (*begD).second == ppin[1]*2 ); ++begD;
    REQUIRE( (*begD).first == ppin[2] );
    REQUIRE( (*begD).second == ppin[2]*2 );
    
     
}

TEST_CASE("Scaling *"){
    vector<int8_t> input{-100, -10, 0, 50, 80};
    vector<int8_t> pin{-50, -5, 0, 25, 40};
    vector<int16_t> input2{ -20000, -50, 0, 50, 20000};

    Audio<int8_t> A(input);
    Audio<int16_t> B(input2);
    Audio< pair<int8_t, int8_t> > P(pin);

    std::pair<float, float> sf = make_pair(2, 2);
    
    Audio<int8_t> Af = A*sf;
    Audio<int16_t> Bf = B*sf;
    Audio< pair<int8_t, int8_t> > Pf = P*sf;

    auto begA = Af.begin(), endA = Af.end();
    auto begB = Bf.begin(), endB = Bf.end();
    auto begP = Pf.begin();

    REQUIRE( *begA == -128 ); ++begA;
    REQUIRE( *begA == -20 ); ++begA;
    REQUIRE( *begA == 0 ); ++begA;
    REQUIRE( *begA == 100 ); ++begA;
    REQUIRE( *begA == 127 );

    REQUIRE( *begB == -32768 ); ++begB;
    REQUIRE( *begB == -100 ); ++begB;
    REQUIRE( *begB == 0 ); ++begB;
    REQUIRE( *begB == 100 ); ++begB;
    REQUIRE( *begB == 32767 ); ++begB;

    REQUIRE( (*begP).first == -100);
    REQUIRE( (*begP).second == -128); ++begP;
    REQUIRE( (*begP).first == -10);
    REQUIRE( (*begP).second == -20); ++begP;
    REQUIRE( (*begP).first == 0);
    REQUIRE( (*begP).second == 0); ++begP;
    REQUIRE( (*begP).first == 50);
    REQUIRE( (*begP).second == 100); ++begP;
    REQUIRE( (*begP).first == 80);
    REQUIRE( (*begP).second == 127); ++begP;
     
}

TEST_CASE("Addition +"){
    vector<int8_t> input{-100, -10, 0, 50, 80};
    vector<int16_t> input2{ -20000, -50, 0, 50, 20000};
    vector<int8_t> pin{-50,-5,0,25,40};

    Audio<int8_t> A(input);
    Audio<int16_t> B(input2);
    Audio<pair<int8_t, int8_t> > P(pin);

    Audio<int8_t> AA = A + A;
    Audio<int16_t> BB = B + B;
    Audio<pair<int8_t, int8_t> > PP = P + P;

    auto begA = AA.begin(), endA = AA.end();
    auto begB = BB.begin(), endB = BB.end();
    auto begP = PP.begin();

    REQUIRE( *begA == -128 ); ++begA;
    REQUIRE( *begA == -20 ); ++begA;
    REQUIRE( *begA == 0 ); ++begA;
    REQUIRE( *begA == 100 ); ++begA;
    REQUIRE( *begA == 127 );

    REQUIRE( *begB == -32768 ); ++begB;
    REQUIRE( *begB == -100 ); ++begB;
    REQUIRE( *begB == 0 ); ++begB;
    REQUIRE( *begB == 100 ); ++begB;
    REQUIRE( *begB == 32767 );

    //vector<int8_t> pin{-50,-50,25,40};      
    //L{-50,-5,0,25,40};
    //R{-100,-10,0,50,80};  
    REQUIRE( (*begP).first == -100);
    REQUIRE( (*begP).second == -128); ++begP;
    REQUIRE( (*begP).first == -10);
    REQUIRE( (*begP).second == -20); ++begP;
    REQUIRE( (*begP).first == 0);
    REQUIRE( (*begP).second == 0); ++begP;
    REQUIRE( (*begP).first == 50);
    REQUIRE( (*begP).second == 100); ++begP;
    REQUIRE( (*begP).first == 80);
    REQUIRE( (*begP).second == 127); ++begP;     
}

TEST_CASE("Range ^ +"){
    vector<int8_t> input{-100, -10, 0, 50, 80, 44, 31};
    vector<int16_t> input2{ -20000, -50, 0, 50, 20000, 43, 100};
    vector<pair<int8_t, int8_t> > pin{make_pair(2,40),
                                      make_pair(0,41),
                                      make_pair(-9,-80),
                                      make_pair(-10,119),
                                      make_pair(-11,120),
                                      make_pair(-12,122),
                                      make_pair(-13,123)
                                      };

    Audio<int8_t> A(input);
    Audio<int16_t> B(input2);
    Audio<pair<int8_t, int8_t> > P(pin);
    
    std::pair<int, int> range = make_pair(2,5);
    Audio<int8_t> AA = A^range;
    Audio<int16_t> BB = B^range;
    Audio<pair<int8_t, int8_t> > PP = P^range;

    auto begA = AA.begin(), endA = AA.end();
    auto begB = BB.begin(), endB = BB.end();
    auto begP = PP.begin();

    REQUIRE( *begA == 0 ); ++begA;
    REQUIRE( *begA == 50 ); ++begA;
    REQUIRE( *begA == 80 ); ++begA;
    REQUIRE( begA == endA );

    REQUIRE( *begB == 0 ); ++begB;
    REQUIRE( *begB == 50 ); ++begB;
    REQUIRE( *begB == 20000 ); ++begB;
    REQUIRE( begB == endB );

    REQUIRE( (*begP).first == pin[2].first );
    REQUIRE( (*begP).second == pin[2].second );  ++begP;
    REQUIRE( (*begP).first == pin[3].first );
    REQUIRE( (*begP).second == pin[3].second );  ++begP;
    REQUIRE( (*begP).first == pin[4].first );
    REQUIRE( (*begP).second == pin[4].second );  ++begP;
     
}

TEST_CASE("Reversal ", ""){
    vector<int8_t> input{48, 49, 50, 51, 52, 53, 54};
    vector<pair<int8_t, int8_t> > pin{make_pair(2,40),
                                      make_pair(0,41),
                                      make_pair(-9,-80)};

    Audio<int8_t> A(input);
    Audio<pair<int8_t, int8_t> > P(pin);

    Audio<int8_t> C = A.reverse();
    Audio<pair<int8_t, int8_t> > Q = P.reverse();

    auto end = C.end();
    auto beg = Q.begin();

    --end;
    REQUIRE( *end == input[0] ); --end;
    REQUIRE( *end == input[1] ); --end;
    REQUIRE( *end == input[2] ); --end;
    REQUIRE( *end == input[3] ); --end;
    REQUIRE( *end == input[4] ); --end;
    REQUIRE( *end == input[5] ); --end;
    REQUIRE( *end == input[6] );

    REQUIRE( (*beg).first == pin[2].first );
    REQUIRE( (*beg).second == pin[2].second ); ++beg;
    REQUIRE( (*beg).first == pin[1].first );
    REQUIRE( (*beg).second == pin[1].second ); ++beg;
    REQUIRE( (*beg).first == pin[0].first );
    REQUIRE( (*beg).second == pin[0].second );


}

TEST_CASE("Range Add", ""){
    vector<int8_t> v  {0, 1, -30, 4 ,5, 60, 7, 8};
    vector<int8_t> v2 {4, 5, -120, 7, 8, 88, 9, 10};

    Audio<int8_t> A(v);
    Audio<int8_t> B(v2);

    std::pair<int, int> range = make_pair(2, 6); //[2,6)

    Audio<int8_t> C = A.rangeAdd(B, range);
    auto beg = C.begin();

    REQUIRE( *beg ==  -128); ++beg;
    REQUIRE( *beg ==  11); ++beg;
    REQUIRE( *beg ==  13); ++beg;
    REQUIRE( *beg ==  127);

}

TEST_CASE("RMS", ""){
    vector<int8_t> v  {0, 1, -30, 4 ,5, 60, 7, 8};
    vector<int16_t> v16  {0, 1, -30, 4 ,5, 60, 7, 8};
    vector<int8_t> v2 {4, 5, -120, 7, 8, 88, 9, 10};

    Audio<int8_t> A(v);
    Audio< pair<int8_t, int8_t> > AA(v);
    Audio< pair<int16_t, int16_t> > A16(v16);
    Audio<int8_t> B(v2);

    float rms1, rms2;
    pair<float, float> rmsp1, rmsp2;

    rms1 = A.RMS();
    float ans1 = 24.122;
    rms2 = B.RMS();
    float ans2 = 53.008;
    rmsp1 = AA.RMS();
    rmsp2 = A16.RMS();
    float ans3 = 48.2442;

    float eps = 0.001;
    REQUIRE( abs(rms1-ans1) < eps );
    REQUIRE( abs(rms2-ans2) < eps );
    REQUIRE( abs(rmsp1.first-ans1) < eps );
    REQUIRE( abs(rmsp1.second-ans3) < eps );
    REQUIRE( abs(rmsp2.first-ans1) < eps );
    REQUIRE( abs(rmsp2.second-ans3) < eps );

}

TEST_CASE("Normalize", ""){
    vector<int8_t> v  {0, 1, -30, 4 ,5, 60, 7, 8};
    vector<int16_t> v2 {-400, -300, 12000, 45 ,5, -660, 7, 8};

    Audio<int8_t> A(v);
    Audio<int16_t> C(v2);
    Audio<std::pair<int8_t, int8_t> > P(v);

    float desRMS = 40.0;
    std::pair<float, float> fdesRMS = make_pair(40.0, 40.0);
    Audio<int8_t> B = A.normalize(desRMS);
    Audio<int16_t> D = C.normalize(desRMS);
    Audio<std::pair<int8_t, int8_t> > Q = P.normalize(fdesRMS);
    std::pair<float, float> Qrms = Q.RMS();
    
    float eps = 1;

    REQUIRE( abs(B.RMS()-desRMS) <eps  );
    REQUIRE( abs(D.RMS()-desRMS) <eps  );
    REQUIRE( abs(Qrms.first-fdesRMS.first) <eps  );
    REQUIRE( abs(Qrms.second-fdesRMS.second) <eps  );

}