/**
* Matteo Kalogirou
* Assignment 5
*
* Header File
* */

#ifndef _samp
#define _samp

#define INT8MAX = 127
#define INT8MIN = -128
#define INT16MAX = 32767
#define INT16MIN = -32768

#include<iostream>
#include<algorithm>
#include<vector>
#include<string>
#include<fstream>
#include<cstdint>       //int8_t and int16_t
#include<utility>       //std::pair
#include<numeric>       //accumulate
#include<functional>    
#include<cmath>         // pow and sqrt()

const static std::string INPUT_PATH = "../sample_input/";
const static std::string OUTPUT_PATH = "../out/";

int assignOpNumber(std::string s);

//-----------------------MONO AUDIO CLASS SPECIALIZATION
template <typename T>
class Audio
{
    private:
        std::vector<T> samples;
        const bool isMono=true;                    //Bool to determine noChannels. mono=true means 1 channel
        int sampleRate;                      //Sample rate
        //Functor
        template<typename TT>
        class normFunctor{
            private:
                float normFactor;
            public:
                //constructor
                normFunctor(float nf) : normFactor(nf) {}
                T operator()(const T& in)
                {
                    float f = std::round((float)in*normFactor);
                    if(sizeof(TT) == 1){
                        if(f > INT8_MAX) f = INT8_MAX;
                        else if(f < INT8_MIN) f = INT8_MIN;
                    }else if(sizeof(TT)==2){
                        if(f > INT16_MAX) f = INT16_MAX;
                        else if(f < INT16_MIN) f = INT16_MIN;}
                    return (TT)f;
                }
        };
    public:
        
    //======Constructor
    Audio(int sr =44100) :  samples(), sampleRate(sr) {}
    Audio(std::string filename, int sr = 44100) : sampleRate(sr){
        loadAudio(filename);
    }
    //      Load from vector
    Audio(std::vector<T> v, int sr = 44100) : sampleRate(sr){
        for(auto i:v)
            samples.push_back(i);
        samples.shrink_to_fit();
    }
    //=====Destructor
    ~Audio(){
        if(!samples.empty())
            samples.clear();
    }
    //=====Copy Constructor
    Audio<T>(const Audio<T> & rhs) : sampleRate(rhs.sampleRate) {
        samples = rhs.samples;
    }
    //=====Copy Assignment
    Audio<T> & operator=(const Audio<T> & rhs){
        sampleRate = rhs.sampleRate;
        samples.clear();
        samples = rhs.samples;
        return *this;
    }
    //=====Move Constructor
    Audio<T>( Audio<T> && rhs) {
        sampleRate = rhs.sampleRate;
        rhs.sampleRate = 0;
        samples = std::move(rhs.samples);
    }
    //=====Move Assignment
    Audio<T> & operator=(Audio<T> && rhs){
        sampleRate = rhs.sampleRate;
        rhs.sampleRate = 0;
        samples = std::move(rhs.samples);
        return *this;
    }

    //====================Iterator=============================
    typename std::vector<T>::iterator begin(void){ return samples.begin(); }
    typename std::vector<T>::iterator end(void){ return samples.end(); }

    /*====================Load Audio===========================
    * Mono audio is of type bitCount. Read in consecutive bytes
    * */
    void loadAudio(std::string filename){
        std::ifstream is(INPUT_PATH+filename, std::ifstream::binary);
        if(is){
            //get the length of the file
            is.seekg(0,is.end);
            int bytes = is.tellg();
            is.seekg(0,is.beg);

            std::cout << "Bytes = " << bytes << std::endl;
            int numberSamples = bytes/(sizeof(T)*1);
            std::cout << "Samples 8bit mono frog  = " << numberSamples << std::endl;
            int seconds = numberSamples/(float)44100;
            std::cout << "Seconds = "<< seconds << std::endl;


            T t;
            char *buffer = new char[sizeof(T)];
            while(is.read(buffer, sizeof(T))){
                if(sizeof(T)==2){
                    t = (buffer[0] << 8) + buffer[1];    
                    samples.push_back(t);
                }else{
                    samples.push_back(T(*buffer));
                }
            }
            delete [] buffer;
 
        }
        is.close();
        samples.shrink_to_fit();
        std::cout << "Size of samples vector = " << samples.size() << std::endl;
    }

    /*====================Save Audio===========================
    * Mono audio is of type bitCount. Strored in consecutive bytes
    * */
    void saveAudio(std::string filename){
        std::string samplingRate = std::to_string(sampleRate);
        std::string sampleSize = "";
        if(sizeof(T) == 1){
            sampleSize = "8";
            std::ofstream ofs(OUTPUT_PATH+filename+"_"+samplingRate+"_"+sampleSize+"_mono.raw", std::ios::binary);
            auto beg = samples.begin(), end = samples.end();
            T t;
            while(beg != end){
                t = *beg;
                ofs << t;
                ++beg;
            }
            ofs.close();
        }
        else{
            sampleSize = "16";
            std::ofstream ofs(OUTPUT_PATH+filename+"_"+samplingRate+"_"+sampleSize+"_mono.raw", std::ofstream::binary);
            auto beg = samples.begin(), end = samples.end();
            T t;
            while(beg != end){
                t = *beg;
                ofs << t;
                ++beg;
            }
            ofs.close();
        }
        
    }

    //======================OVERLOADS===========================

    /**---Concatenation |
     * Add two signals together which are the same type and size
     * */
    Audio<T> operator|(const Audio<T> &rhs){
        //Create new signal 
        Audio<T> output(*this);
        //Use STL
        output.samples.insert(output.samples.end(), rhs.samples.begin(), rhs.samples.end());
        return output;
    }

    /**---Volume Scaling *std::pair<float, float>
     *  Multiply each signal by the float factor. Mono - only first factor used
     * */
    Audio<T> operator*(const std::pair<float, float> scaleFactor){
        //Create new Audio signal from the original
        Audio<T> output(*this);
        float f;
        auto beg = output.begin(), end = output.end();

        while(beg != end){
            f = scaleFactor.first * (*beg); //Get the scaled sample
            //Check that samples is within limits
            if(sizeof(T) ==1 )          //int8_t
            {
                if(f > INT8_MAX)
                    f = INT8_MAX;
                else if( f < INT8_MIN)
                    f = INT8_MIN;
                *beg = (T)f;
            }else if(sizeof(T) ==2){    //int16_t
                if(f > INT16_MAX)
                    f = INT16_MAX;
                else if( f < INT16_MIN)
                    f = INT16_MIN;
                *beg = (T)f;
            }
            //Assign the scaled value
            ++beg;
        }
        return output;
    }

    /**---Vector addition
     *  Add each element of the audio signal. Output is clamped.
     * */
    Audio<T> operator+(const Audio<T> &rhs){
        Audio<T> output(*this);

        auto rhsBeg = rhs.samples.begin(), rhsEnd = rhs.samples.end();
        auto outBeg = output.samples.begin();

        int t;  //temp to store result

        while(rhsBeg != rhsEnd){
            
            t = *rhsBeg + *outBeg;
            if( sizeof(T) == 1 ){       //int8
                if(t > INT8_MAX)
                    *outBeg = INT8_MAX;
                else if(t < INT8_MIN)
                    *outBeg = INT8_MIN;
                else
                    *outBeg = (T)t;
            }else if( sizeof(T) == 2){  //int16
                if(t > INT16_MAX)
                    *outBeg = INT16_MAX;
                else if(t < INT16_MIN)
                    *outBeg = INT16_MIN;
                else
                    *outBeg = (T)t;
            }

            ++rhsBeg;
            ++outBeg;
        }
        return output;
    }

    /**---Range Selection
     *  Create a new audio clip from a selection of the original 
     * */
    Audio<T> operator^(const std::pair<int, int> range){
        
        std::vector<T> clip{};
        clip.reserve(range.second-range.first);
        auto beg = &(samples[range.first]);
        auto end = &(samples[range.second]);        

        while( beg != end){
            clip.push_back(*beg);
            ++beg;
        }
        Audio<T> output(clip);
        return output;

    }

    //======================STL FUNCTIONS===========================

    //---Reverse
    Audio<T> reverse(){
        Audio<T> output(*this);
        std::reverse(output.samples.begin(), output.samples.end());
        return output;
    }

    /**---Range Add
     * Extracts a defined range of each audio file, copies this range into a temporary variable and then
     * adds the results together.
     * std::copy(Iterator inputStart, Iterator inputEnd, Iterator OutputStart)
     * */
    Audio<T> rangeAdd(const Audio<T> &rhs, const std::pair<int, int> range){

        int clip_length = range.second -range.first;
        Audio<T> clip1; clip1.samples.resize(clip_length);
        Audio<T> clip2; clip2.samples.resize(clip_length);

        //Get iterators to the temp signals
        auto clip1Beg = clip1.samples.begin();
        auto clip2Beg = clip2.samples.begin();

        //Get iterators for the defined range of input signals
        auto in1Beg = &(samples[range.first]), in1End = &(samples[range.second]);
        auto in2Beg = &(rhs.samples[range.first]), in2End = &(rhs.samples[range.second]);

        //perform Copy into the clips
        std::copy(in1Beg, in1End, clip1Beg);
        std::copy(in2Beg, in2End, clip2Beg);

        //Add the clips together using the + operator
        Audio<T> output = clip1 + clip2;
        return output;
    }

    /**---Root Mean Square
     * Compute the RMS of the samples and return the result. Using STL
     * std::accumulate<Iterator inStart, Iterator inEnd, T initial value>
     * */
    float RMS(void){
        auto sum = std::accumulate(samples.begin(),
                                   samples.end(),
                                   0.0,
                                   [](float f, T t){ return f + float(t)*float(t);} );
        return sqrt(sum/samples.size());
    }

    /**---Normalize
     * Input: RMSdesired, RMScurrent, samples.
     * Output: Normalised RMS
     * Compute the current RMS of the signal, then for each element apply the conversion output = input*(RMSd/RMSc);
     * std::transform<Iterator inStart, Iterator inEnd, Iterator outStart, Lambda >
     * */
    Audio<T> normalize(const float RMSdes){
        float RMScur = this->RMS();
        float normfactor = RMSdes/RMScur;

        std::vector<T> v {};
        v.resize(samples.size());

        std::transform( samples.begin(),
                        samples.end(),
                        v.begin(),
                        normFunctor<T>(normfactor) );
        Audio<T> output(v);
        return output;
    }



    //Helper Functions
    T get(int pos){
        if(pos < samples.size())
            return samples[pos];
        else
            return 0;
    }
    std::vector<T> getVector()const{
        return samples;
    }

};


//========================================================================
//-----------------------STEREO AUDIO CLASS SPECIALIZATION
//========================================================================


template <typename T>
class Audio< std::pair<T, T> >
{
    private:
        std::vector< std::pair<T, T> > samples;
        const bool isMono=false;              //Bool to determine noChannels. mono=true means 1 channel
        int sampleRate;                      //Sample rate
        //Functor
        template<typename TT>
        class normFunctor{
            private:
                std::pair<float,float> normFactor;
            public:
                //constructor
                normFunctor(std::pair<float,float> nf): normFactor(nf) {}
                std::pair<TT,TT> operator()(const std::pair<T,T> &in)
                {
                    float fleft = std::round((float)in.first*normFactor.first);
                    float fright = std::round((float)in.second*normFactor.second);
                    if(sizeof(TT) == 1){
                        if(fleft > INT8_MAX) fleft = INT8_MAX;
                        else if(fleft < INT8_MIN) fleft = INT8_MIN;
                        if(fright > INT8_MAX) fright = INT8_MAX;
                        else if(fright < INT8_MIN) fright = INT8_MIN;
                    }else if(sizeof(TT)==2){
                        if(fleft > INT16_MAX) fleft = INT16_MAX;
                        else if(fleft < INT16_MIN) fleft = INT16_MIN;
                        if(fright > INT16_MAX) fright = INT16_MAX;
                        else if(fright < INT16_MIN) fright = INT16_MIN;
                    }
                    return std::make_pair( (TT)fleft, (TT)fright );
                }
        };
    public:

     //======Constructor
    Audio(std::string filename, int sr = 44100) : sampleRate(sr){
        loadAudio(filename);
    }
    Audio(int sr = 44100) : sampleRate(sr){ }
    Audio(std::vector<std::pair<T,T> > v, int sr=44100) : sampleRate(sr), samples(){
        samples.resize(v.size());
        samples = std::move(v);
    }
    //      Load from vector
    Audio(std::vector<T> v, int sr = 44100) : sampleRate(sr){
        for(auto i:v){
            samples.push_back(std::make_pair(i,2*i));
        }
        samples.shrink_to_fit();
    }
        //=====Destructor
    ~Audio(){
        if(!samples.empty())
            samples.clear();
    }
    //=====Copy Constructor
    Audio< std::pair<T, T> >(const Audio< std::pair<T, T> > & rhs) : sampleRate(rhs.sampleRate) {
        samples = rhs.samples;
    }
    //=====Copy Assignment
    Audio< std::pair<T, T> > & operator=(const Audio< std::pair<T, T> > & rhs){
        sampleRate = rhs.sampleRate;
        samples.clear();
        samples = rhs.samples;
        return *this;
    }
    //=====Move Constructor
    Audio< std::pair<T, T> >( Audio< std::pair<T, T> > && rhs) {
        sampleRate = rhs.sampleRate;
        rhs.sampleRate = 0;
        samples = std::move(rhs.samples);
    }
    //=====Move Assignment
    Audio< std::pair<T, T> > & operator=(Audio< std::pair<T, T> > && rhs){
        sampleRate = rhs.sampleRate;
        rhs.sampleRate = 0;
        samples = std::move(rhs.samples);
        return *this;
    }

    T getL(int pos){ return samples[pos].first; }
    T getR(int pos){ return samples[pos].second; }

    //====================Iterator=============================
    class iterator{
        private:
            std::pair<T, T> *ptr;
            iterator( std::pair<T, T> *p ) : ptr(p){}
        public:
            std::pair<T, T> operator*(){
                return *ptr;
            }
            iterator &operator++(){
                ++ptr;
                return *this;
            }
            iterator &operator--(){
                --ptr;
                return *this;
            }

            bool operator !=(const iterator & rhs){
                if(ptr != rhs.ptr)
                    return true;
                else
                    return false;
            }

            bool operator==(const iterator &rhs){
                if(ptr == rhs.ptr)
                    return true;
                else
                    return false;
            }

            void operator=(const std::pair<T, T> val){
                (*ptr).first = val.first;
                (*ptr).second = val.second;
            }
        friend Audio;
    };

    iterator begin(void){ return iterator( &(samples[0]) ); }
    iterator end(void){ return iterator( &(samples[samples.size()])); }

    /*====================Load Audio===========================
    * Mono audio is of type bitCount. Read in consecutive bytes
    * */
    void loadAudio(std::string filename){
        std::ifstream is(INPUT_PATH+filename, std::ifstream::binary);
        if(is){
            //get the length of the file
            is.seekg(0,is.end);
            int bytes = is.tellg();
            is.seekg(0,is.beg);

            std::cout << "Stereo Bytes = " << bytes << std::endl;
            int numberSamples = bytes/(sizeof(T)*2);
            std::cout << "Stereo Samples = " << numberSamples << std::endl;
            int seconds = numberSamples/(float)44100;
            std::cout << "Seconds = "<< seconds << std::endl;

            T tl, tr;
            char *lbuff = new char[sizeof(T)];
            char *rbuff = new char[sizeof(T)];
            while(!is.eof()){
                if(sizeof(T)==2){
                    is.read(lbuff, sizeof(T));
                    tl = (lbuff[0]<<8) + lbuff[1];
                    is.read(rbuff, sizeof(T));
                    tr = (rbuff[0]<<8) + rbuff[1];
                    samples.push_back( std::make_pair( tl, tr ) );
                }else{
                    is.read(rbuff, sizeof(T));
                    is.read(lbuff, sizeof(T));
                    samples.push_back( std::make_pair( T(*lbuff), T(*rbuff)) );  
                }
            }
            delete [] lbuff; delete [] rbuff;

        }
        is.close();
        samples.shrink_to_fit();
        std::cout << "Size of samples vector = " << samples.size() << std::endl;
    }

    /*====================Save Audio===========================
    * Mono audio is of type bitCount. Strored in consecutive bytes
    * */
    void saveAudio(std::string filename){
        std::string samplingRate = std::to_string(sampleRate);
        std::string sampleSize = "";
        if(sizeof(T) == 1){
            sampleSize = "8";
            std::ofstream ofs(OUTPUT_PATH+filename+"_"+samplingRate+"_"+sampleSize+"_stereo.raw", std::ios::binary);
            auto beg = this->begin(), end = this->end();
            T l, r;
            while(beg != end){
                l = (*beg).first; r = (*beg).second;
                ofs << l << r;
                ++beg;
            }
            ofs.close();
        }
        else{
            sampleSize = "16";
            std::ofstream ofs(OUTPUT_PATH+filename+"_"+samplingRate+"_"+sampleSize+"_mono.raw", std::ofstream::binary);
            auto beg = this->begin(), end = this->end();
            T l, r;
            while(beg != end){
                l = (*beg).first; r = (*beg).second;
                ofs << l << r;
                ++beg;
            }
            ofs.close();
        }
        
    }

    //======================OVERLOADS===========================

    /**---Concatenation |
     * Add two signals together which are the same type and size
     * */
    Audio< std::pair<T,T> > operator|(const Audio<std::pair<T,T> > &rhs){
        //Create new signal 
        Audio< std::pair<T,T> > output(*this);
        //Use STL
        output.samples.insert(output.samples.end(), rhs.samples.begin(), rhs.samples.end());
        return output;
    }

    /**---Volume Scaling *std::pair<float, float>
     *  Multiply each signal by the float factor.
     * */
    Audio< std::pair<T,T> > operator*(const std::pair<float, float> scaleFactor){
        //Create new Audio signal from the original
        Audio<std::pair<T,T> > output(*this);
        float fleft, fright;
        std::pair<T,T> val;
        auto beg = output.begin(), end = output.end();

        while(beg != end){
            fleft = scaleFactor.first * (*beg).first; //Get the scaled sample
            fright = scaleFactor.second * (*beg).second; //Get the scaled sample
            //Check that samples is within limits
            if(sizeof(T) ==1 )          //int8_t
            {
                if(fleft > INT8_MAX)
                    fleft = INT8_MAX;
                else if( fleft < INT8_MIN)
                    fleft = INT8_MIN;
                if(fright > INT8_MAX)
                    fright = INT8_MAX;
                else if( fright < INT8_MIN)
                    fright = INT8_MIN;
            }else if(sizeof(T) ==2){    //int16_t
                if(fleft > INT16_MAX)
                    fleft = INT16_MAX;
                else if( fleft < INT16_MIN)
                    fleft = INT16_MIN;
                if(fright > INT16_MAX)
                    fright = INT16_MAX;
                else if( fright < INT16_MIN)
                    fright = INT16_MIN;
            }
            //Assign the scaled value
            val = std::make_pair( (T)fleft, (T)fright );
            beg = val;
            ++beg;
        }
        return output;
    }

    /**---Vector addition
     *  Add each element of the audio signal. Output is clamped.
     * */
    Audio<std::pair<T,T> > operator+( Audio<std::pair<T,T> > &rhs){
        Audio<std::pair<T,T> > output(*this);

        auto rhsBeg = rhs.begin(), rhsEnd = rhs.end();
        auto outBeg = output.begin();

        std::pair< int, int> t;  //temp to store result

        while(rhsBeg != rhsEnd){
            t = std::make_pair( (*rhsBeg).first + (*outBeg).first, (*rhsBeg).second + (*outBeg).second);
            if( sizeof(T) == 1 ){       //int8
                if(t.first > INT8_MAX)
                    t.first = INT8_MAX;
                else if(t.first < INT8_MIN)
                    t.first = INT8_MIN;
                if(t.second > INT8_MAX)
                    t.second = INT8_MAX;
                else if(t.second < INT8_MIN)
                    t.second = INT8_MIN;
            }else if( sizeof(T) == 2){  //int16
                if(t.first > INT16_MAX)
                    t.first = INT16_MAX;
                else if(t.first < INT16_MIN)
                    t.first = INT16_MIN;
                if(t.second > INT16_MAX)
                    t.second = INT16_MAX;
                else if(t.second < INT16_MIN)
                    t.second = INT16_MIN;
            }
        outBeg = std::make_pair((T)t.first, (T)t.second );
        
        ++rhsBeg;
        ++outBeg;
        }
        return output;
    }

    /**---Range Selection
     *  Create a new audio clip from a selection of the original 
     * */
    Audio<std::pair<T,T> > operator^(const std::pair<int, int> range){
        
        std::vector<std::pair<T,T> > clip{};
        clip.reserve(range.second-range.first);
        auto beg = &(samples[range.first]);
        auto end = &(samples[range.second]);        

        while( beg != end){
            clip.push_back(*beg);
            ++beg;
        }
        Audio<std::pair<T,T> > output(clip);
        return output;
    }

    //======================STL FUNCTIONS===========================

    //---Reverse
    Audio<std::pair<T,T> > reverse(){
        Audio<std::pair<T,T> > output(*this);
        std::reverse(output.samples.begin(), output.samples.end());
        return output;
    }

    /**---Range Add
     * Extracts a defined range of each audio file, copies this range into a temporary variable and then
     * adds the results together.
     * std::copy(Iterator inputStart, Iterator inputEnd, Iterator OutputStart)
     * */
    Audio<std::pair<T,T> > rangeAdd(const Audio<std::pair<T,T> > &rhs, const std::pair<int, int> range){

        int clip_length = range.second -range.first;
        Audio<std::pair<T,T> > clip1; clip1.samples.resize(clip_length);
        Audio<std::pair<T,T> > clip2; clip2.samples.resize(clip_length);

        //Get iterators to the temp signals
        auto clip1Beg = clip1.begin();
        auto clip2Beg = clip2.begin();

        //Get iterators for the defined range of input signals
        auto in1Beg = &(samples[range.first]), in1End = &(samples[range.second]);
        auto in2Beg = &(rhs.samples[range.first]), in2End = &(rhs.samples[range.second]);

        //perform Copy into the clips
        std::copy(in1Beg, in1End, clip1Beg);
        std::copy(in2Beg, in2End, clip2Beg);

        //Add the clips together using the + operator
        Audio<T> output = clip1 + clip2;
        return output;
    }

    /**---Root Mean Square
     * Compute the RMS of the samples and return the result. Using STL
     * std::accumulate<Iterator inStart, Iterator inEnd, T initial value>
     * */
    std::pair<float,float> RMS(void){
        auto sumLeft = std::accumulate(samples.begin(),
                                   samples.end(),
                                   0.0,
                                   [](float f, std::pair<T,T> t){ 
                                       return f + float(t.first)*float(t.first);
                                    } );
        auto sumRight = std::accumulate(samples.begin(),
                                   samples.end(),
                                   0.0,
                                   [](float f, std::pair<T,T> t){ 
                                       return f + float(t.second)*float(t.second);
                                    } );

        return std::make_pair(std::sqrt(sumLeft/samples.size()) , std::sqrt(sumRight/samples.size()));
    }

    /**---Normalize
     * Input: RMSdesired, RMScurrent, samples.
     * Output: Normalised RMS
     * Compute the current RMS of the signal, then for each element apply the conversion output = input*(RMSd/RMSc);
     * std::transform<Iterator inStart, Iterator inEnd, Iterator outStart, Lambda >
     * */
    Audio<std::pair<T,T> > normalize(const std::pair<float,float> RMSdes){
        std::pair<float,float> RMScur = this->RMS();
        std::pair<float,float> normfactor = std::make_pair( RMSdes.first/RMScur.first, RMSdes.second/RMScur.second );

        std::vector<std::pair<T,T> > v {};
        v.resize(samples.size());

        std::transform( samples.begin(),
                        samples.end(),
                        v.begin(),
                        normFunctor<T>(normfactor) );
        Audio<std::pair<T,T> > output(v);
        return output;
    }



};





#endif