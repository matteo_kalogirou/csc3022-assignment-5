=========================================================================== KLGMAT001

This is a simple sound editting program. The focus is on generic type templating.
The program allows for simple operatins such as concatenation, reversal
and superposition of soundfiles. Both stereo and mono soundfiles are 
supported. For details of operation see below:

Input files are of type *.raw and must be stored in /sample_input
All outputs are stored in /out

NB when specifying input filenames you must include the extensions
    -eg someInput.raw
When specifying the output 

To run the program: first compile using make in /src
and then run using 

./driver -r sampleRateInHz -b bitCount -c noChannels [-o outFileName ] [<ops>] soundFile1 [soundFile2]

Where the ops are:
• -add: add soundFile1 and soundFile2
• -cut r1 r2: remove samples over range [r1,r2] (inclusive) r1 r2 are ints
• -radd r1 r2 s1 s2 : add soundFile1 and soundFile2 over sub-ranges indicated
(in seconds). The ranges must be equal in length.
• -cat: concatenate soundFile1 and soundFile2
• -v r1 r2: volume factor for left/right audio. r1/2 are floats.
• -rev: reverse sound file (assumes one sound file only)
• -rms: Prints out the RMS of the sound file.
• -norm r1 r2: normalize file for left/right audio. r1 r2 are floats


To run the test cses: compile using make test
and then run using ./test


Note:
I could not get the int16_t to output correctly. According to the unit test the class works just fine though.
Did not implement the fading.