CC=g++								#Compiler
FLAGS= --std=c++11
CXXFLAGS= -Wall			#Headers + all warnings

driver:	driver.o samp.h samp.o
	$(CC) driver.o samp.o -o driver

test: test.o catch.hpp samp.h samp.o
	$(CC) test.o samp.o -o test

%.o: %.cpp
	$(CC) -c $(CXXFLAGS) $< $(FLAGS)

clean:
	rm -f *.o driver test