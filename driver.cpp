/**
* Matteo Kalogirou
* Assignment 5
*
* Driver file
* */

#include "samp.h"

using namespace std;

int main( int argc, char* argv[])
{

        //Read in the input arguments    
        int sampleRate = stoi(argv[2]);
        int bitCount = stoi(argv[4]);
        int noChannels = stoi(argv[6]);
        string outFileName = argv[8];
        string ops = argv[9];
        int opNumber = assignOpNumber(ops);

        std::pair<int,int> range = std::make_pair(1,20);
        std::pair<float,float> vf = std::make_pair(1,1);
        std::pair<float,float> desRMS = std::make_pair(10,10);
        std::pair<float, float> rms;

        string soundFile1;
        string soundFile2;

        switch(opNumber){
            case 0: //Add
                soundFile1 = argv[10];
                soundFile2 = argv[11];
                cout << "Add " << soundFile1 << "+" << soundFile2 << endl;
                break;
            case 1: //Cut
                range.first = stoi(argv[10]);
                range.second = stoi(argv[11]);
                soundFile1 = argv[12];
                cout << "Cut " << soundFile1<< endl;
                break;
            case 2: //Range Add
                range.first = stoi(argv[10]);
                range.second = stoi(argv[11]);
                soundFile1 = argv[12];
                soundFile2 = argv[13];
                cout << "Range Add " << soundFile1 << "+" << soundFile2 << endl;
                break;
            case 3: // Cat
                soundFile1 = argv[10];
                soundFile2 = argv[11];
                cout << "Cat " << soundFile1 << "+" << soundFile2 << endl;
                break;
            case 4: //Volume factor
                vf.first = stof(argv[10]);
                vf.second = stof(argv[11]);
                soundFile1 = argv[12];
                cout << "Volume Factor: " << soundFile1<< endl;
                break;
            case 5: //reverse
                soundFile1 = argv[10];
                cout << "Reverse " << soundFile1 << endl;
                break;
            case 6: //Print RMS
                soundFile1 = argv[10];
                cout << "Calculate RMS" << soundFile1 << endl;
                break;
            case 7://norm
                desRMS.first = stof(argv[10]);
                desRMS.second = stof(argv[11]);
                soundFile1 = argv[12];
                cout << "Normalise " << soundFile1 << endl;
                break;
            case 8:
                cout << "Invalid operation." << endl;
                break;
        }


    if(noChannels == 1 && bitCount ==8){ //mono 8bit
        Audio<int8_t> A(soundFile1);
        Audio<int8_t> B(soundFile1);
        Audio<int8_t> C;
        switch(opNumber){
            case 0: //Add
                C = A + B;
                C.saveAudio(outFileName);
                break;
            case 1: //Cut
                C = A^range;
                C.saveAudio(outFileName);
                break;
            case 2: //Range Add
                C = (A^range) + (B^range);
                C.saveAudio(outFileName);
                break;
            case 3: //cat
                C = A | B;
                break;
            case 4: //Volume factor
                C = A*vf;
                C.saveAudio(outFileName);
                break;
            case 5: //reverse
                C = A.reverse();
                C.saveAudio(outFileName);
                break;
            case 6: //Print RMS
                cout << "RMS of " << soundFile1 << " = " << A.RMS() << endl; 
                break;
            case 7://norm
                C = A.normalize(desRMS.first);
                C.saveAudio(outFileName);
                break;
            case 8:
                cout << "Invalid operation." << endl;
        }
    }

    else if(noChannels == 1 && bitCount ==16){ //mono 16bit
        Audio<int16_t> A(soundFile1);
        Audio<int16_t> B(soundFile1);
        Audio<int16_t> C;
        switch(opNumber){
            case 0: //Add
                C = A + B;
                C.saveAudio(outFileName);
                break;
            case 1: //Cut
                C = A^range;
                C.saveAudio(outFileName);
                break;
            case 2: //Range Add
                C = (A^range) + (B^range);
                C.saveAudio(outFileName);
                break;
            case 3: //cat
                C = A | B;
                break;
            case 4: //Volume factor
                C = A*vf;
                C.saveAudio(outFileName);
                break;
            case 5: //reverse
                C = A.reverse();
                C.saveAudio(outFileName);
                break;
            case 6: //Print RMS
                cout << "RMS of " << soundFile1 << " = " << A.RMS() << endl; 
                break;
            case 7://norm
                C = A.normalize(desRMS.first);
                C.saveAudio(outFileName);
                break;
            case 8:
                cout << "Invalid operation." << endl;
        }
    }

    else if(noChannels == 2 && bitCount ==8){ //Stereo 8bit
        Audio<pair<int8_t,int8_t> > A(soundFile1);
        Audio<pair<int8_t,int8_t> > B(soundFile1);
        Audio<pair<int8_t,int8_t> > C;
        switch(opNumber){
            case 0: //Add
                C = A + B;
                C.saveAudio(outFileName);
                break;
            case 1: //Cut
                C = A^range;
                C.saveAudio(outFileName);
                break;
            case 2: //Range Add
                A = A^range; B = B^range;
                C = A + B;
                C.saveAudio(outFileName);
                break;
            case 3: //cat
                C = A | B;
                break;
            case 4: //Volume factor
                C = A*vf;
                C.saveAudio(outFileName);
                break;
            case 5: //reverse
                C = A.reverse();
                C.saveAudio(outFileName);
                break;
            case 6: //Print RMS
                rms = A.RMS();
                cout << "RMS of " << soundFile1 << " = " << rms.first << ","<< rms.second << endl; 
                break;
            case 7://norm
                C = A.normalize(desRMS);
                C.saveAudio(outFileName);
                break;
            case 8:
                cout << "Invalid operation." << endl;
        }
    }

    else if(noChannels == 2 && bitCount ==16){ //Stereo 8bit
        Audio<pair<int16_t,int16_t> > A(soundFile1);
        Audio<pair<int16_t,int16_t> > B(soundFile1);
        Audio<pair<int16_t,int16_t> > C;
        switch(opNumber){
            case 0: //Add
                C = A + B;
                C.saveAudio(outFileName);
                break;
            case 1: //Cut
                C = A^range;
                C.saveAudio(outFileName);
                break;
            case 2: //Range Add
                A = A^range; B = B^range;
                C = A + B;
                C.saveAudio(outFileName);
                break;
            case 3: //cat
                C = A | B;
                break;
            case 4: //Volume factor
                C = A*vf;
                C.saveAudio(outFileName);
                break;
            case 5: //reverse
                C = A.reverse();
                C.saveAudio(outFileName);
                break;
            case 6: //Print RMS
                rms = A.RMS();
                cout << "RMS of " << soundFile1 << " = " << rms.first << ","<< rms.second << endl; 
                break;
            case 7://norm
                C = A.normalize(desRMS);
                C.saveAudio(outFileName);
                break;
            case 8:
                cout << "Invalid operation." << endl;
        }
    }
    
    return 0;
}

int assignOpNumber(string s){
    if(s.compare("-add") == 0)
        return 0;
    else if(s.compare("-cut") == 0 )
        return 1;
    else if(s.compare("-radd") == 0)
        return 2;
    else if(s.compare("-cat") == 0)
        return 3;
    else if(s.compare("-v") == 0)
        return 4;
    else if(s.compare("-rev") == 0)
        return 5;
    else if(s.compare("-rms") == 0)
        return 6;
    else if(s.compare("-norm") == 0)
        return 7;
    else return 8;
}